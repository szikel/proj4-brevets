"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
   
    max_speed = 10000

    hour_sum = 0
    minute_sum = 0

    control = control_dist_km

    if control <= 200:
        max_speed = 34.0
        hour_sum += int((control // max_speed))
        minute_sum += int(round(((control_dist_km / max_speed) % 1) * 60))
        opening_time_arw = arrow.get(brevet_start_time)
        opening_time = opening_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)
        return opening_time.isoformat()

    elif control > 200:
        max_speed = 34
        hour_sum += int((200 // max_speed))
        minute_sum += int(((200 / max_speed) % 1) * 60)

    if control <= 400:
        max_speed = 32
        hour_sum += int(((control-200) // max_speed))
        minute_sum += int(round((((control_dist_km-200) / max_speed) % 1) * 60))
        opening_time_arw = arrow.get(brevet_start_time)
        opening_time = opening_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)
        return opening_time.isoformat()

    elif control > 400:
        max_speed = 30
        hour_sum += int((200 // max_speed))
        minute_sum += int(round(((200 / max_speed) % 1) * 60))

    if control <= 600:
        max_speed = 30
        hour_sum += int(((control-400)// max_speed))
        minute_sum += int(round((((control_dist_km-400) / max_speed) % 1) * 60))
        opening_time_arw = arrow.get(brevet_start_time)
        opening_time = opening_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)
        return opening_time.isoformat()

    elif control > 600:
        max_speed = 30 
        hour_sum += int((200 // max_speed))
        minute_sum += int(round(((200 / max_speed) % 1) * 60))

    if control_dist_km >= 1000:
        max_speed = 28
        hour_sum += int(((control-800) // max_speed))
        minute_sum += int(round((((control_dist_km-800)/ max_speed) % 1) * 60))


    opening_time_arw = arrow.get(brevet_start_time)
    opening_time = opening_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)
    
    return opening_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """    
    min_speed = 10000

    hour_sum = 0
    minute_sum = 0
    
    control = control_dist_km
    
    if control <= 200:
        max_speed = 15.0
        hour_sum += int((control // max_speed))
        minute_sum += int(round(((control_dist_km / max_speed) % 1) * 60))
        opening_time_arw = arrow.get(brevet_start_time)
        opening_time = opening_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)
        return opening_time.isoformat()

    elif control > 200:
        max_speed = 15
        hour_sum += int((200 // max_speed))
        minute_sum += int(((200 / max_speed) % 1) * 60)
    
    if control <= 400:
        max_speed = 15
        hour_sum += int(((control-200) // max_speed))
        minute_sum += int(round((((control_dist_km-200) / max_speed) % 1) * 60))
        opening_time_arw = arrow.get(brevet_start_time)
        opening_time = opening_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)
        return opening_time.isoformat()

    elif control > 400:
        max_speed = 15
        hour_sum += int((200 // max_speed))
        minute_sum += int(round(((200 / max_speed) % 1) * 60))
    
    if control <= 600:
        max_speed = 15
        hour_sum += int(((control-400) // max_speed))
        minute_sum += int(round((((control_dist_km-400)/ max_speed) % 1) * 60))
        opening_time_arw = arrow.get(brevet_start_time)
        opening_time = opening_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)
        return opening_time.isoformat()

    elif control > 600:
        max_speed = 11.428
        hour_sum += int((200 // max_speed))
        minute_sum += int(round(((200 / max_speed) % 1) * 60))
    
    if control_dist_km == 1000:
        max_speed = 11.428
        hour_sum += int(((control-800) // max_speed))
        minute_sum += int(round((((control_dist_km-800) / max_speed) % 1) * 60))

    closing_time_arw = arrow.get(brevet_start_time)
    closing_time = closing_time_arw.shift(hours=+hour_sum,minutes=+minute_sum)


    return closing_time.isoformat()

