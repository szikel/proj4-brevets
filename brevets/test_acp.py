import nose
import acp_times
import arrow

def test_sanity_zero():
    initial_date = arrow.get('2017-01-01T00:00:00+00:00')
    assert acp_times.open_time(0,200,initial_date.isoformat()) == initial_date.isoformat()
    assert acp_times.close_time(0,200,initial_date.isoformat()) == initial_date.isoformat()

def test_under_200():
    initial_date = arrow.get('2017-01-01T00:00:00+00:00')
    assert acp_times.open_time(150,200,initial_date.isoformat()) == initial_date.shift(hours=4,minutes=25).isoformat()
    assert acp_times.close_time(150,200,initial_date.isoformat()) == initial_date.shift(hours=10,minutes=0).isoformat()

def test_at_200():
    initial_date = arrow.get('2017-01-01T00:00:00+00:00')
    assert acp_times.open_time(200,200,initial_date.isoformat()) == initial_date.shift(hours=5,minutes=53).isoformat()
    assert acp_times.close_time(200,200,initial_date.isoformat()) == initial_date.shift(hours=13,minutes=20).isoformat()

def test_under_400():
    initial_date = arrow.get('2017-01-01T00:00:00+00:00')
    assert acp_times.open_time(399,400,initial_date.isoformat()) == initial_date.shift(hours=12,minutes=5).isoformat()
    assert acp_times.close_time(399,400,initial_date.isoformat()) == initial_date.shift(hours=24+2,minutes=36).isoformat()

def test_closing_time_at_600():
    initial_date = arrow.get('2017-01-01T00:00:00+00:00')
    assert acp_times.close_time(600,600,initial_date.isoformat()) == initial_date.shift(hours=24+16,minutes=0).isoformat()

